# convert qwerty ergovim vimrc mappings to specicic keyboard layout mapping
# mapping is dvorak with the u and i reversed
# 
map = {
# '-' => '[',
'=' => ']',
'q' => '\'',
'w' => ',',
'e' => '.',
'r' => 'p',
't' => 'y',
'y' => 'f',
'u' => 'g',
'i' => 'c',
'o' => 'r',
'p' => 'l',
'[' => '/',
']' => '=',
'a' => 'a',
's' => 'o',
'd' => 'e',
'f' => 'i',
'g' => 'u',
'h' => 'd',
'j' => 'h',
'k' => 't',
'l' => 'n',
';' => 's',
'z' => ';',
'x' => 'q',
'c' => 'j',
'v' => 'k',
'b' => 'x',
'n' => 'b',
'm' => 'm',
',' => 'w',
'.' => 'v',
'/' => 'z'}




text = File.open('ergovim')
text.each_line do |line|
  if !line.include? '"' 
    values = line.split(' ')
    if !(values[1].to_s.include? 'Down' or values[1].to_s.include? 'Up' or values[1].to_s.include? 'Left' or values[1].to_s.include? 'Right' or values[1].to_s.include? 'Home' or values[1].to_s.include? 'End' or values[1].to_s.include? 'Bar' or values[1].to_s.include? 'Space')
      map.each_pair {|f,t| values[1].to_s.gsub! f, t}
    end
    puts "#{values[0]} #{values[1]} #{values[2]}"
  else line.include? '"'
    puts line
  end
end

text.close

